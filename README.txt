(c) Nigel Gibbs, 2006-2008

Description:
============

Phuzby is a small lightweight cross-platform phone book tool written in Java which sits in the task tray and allows quick and easy access to LDAP enabled directories.  

It's easy... you right-click on the Phuzby icon, type in part of the name of a colleague, customer or supplier and Phuzby will return a list of matches, along with their photos where available. Phuzby can also read out loud the telephone number for you to help you dial it; or it can even dial it for you if your phone supports TDFM audible codes.

Requirements:
=============
Requires Java 1.5.x (http://www.sun.com) 
Requires Systray4j (http://systray.sourceforge.net)

Building:
=========
Modify the LDAP settings in Main.java and then compile and build project. You may have to copy the systray4j.dll and systray4j.jar files along with the icon files into the distribution folder after building before the application will run. The task tray icon supported on Windows and Linux in KDE3

Installation:
=============
Once compiled, copy the distribution folder to your hard drive and double click on the JAR file. You might want to add it to your start-up group.

Changes:
========
v1.0 Inital release
v1.1 Got the photos working and the sound
v1.2 Put the radio buttons for suppliers etc on the search form
v1.3 Now only contacts from the Global address list are displayed
v1.41 Code clean up and initial public GNU release

Contact:
========
You can contact the author via email to nigel@gibbsoft.com, or via http://gibbsoft.com.

