/*
 * Main.java
 *
 * Created on 28 March 2006, 21:15
 * Copyright 2006-2008 Nigel Gibbs
 * 
 *  This file is part of Phuzby.
 *
 *  Phuzby is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Phuzby is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package com.gibbsoft.phuzby;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;

public class Main extends JFrame implements ActionListener {
    // frame dimension
    static final int INIT_WIDTH = 240;
    static final int INIT_HEIGHT = 90;
    public static final String BVERSION = "v1.42";
    public Dialog1 dlg;
    private Wait waitWindow;
    public static LdapSettings ldapSettings = new LdapSettings();
    final SystemTray tray = SystemTray.getSystemTray();
    final TrayIcon trayIcon =
            new TrayIcon(createImage("icons/phuzby16x16t.gif", "tray icon"));

    public Main() {
        ////////////////////////////////////////////
        // Set your LDAP connection details here :
        
        // URL for LDAP server
        ldapSettings.providerURL = "ldap://skywalker:389/cn=Users,DC=gibbsoft,DC=local";
        
        // Type of authentication
        ldapSettings.authenticationType="simple";
        
        // Specify the DN of a User who has browse rights to the LDAP tree
        ldapSettings.loginUser="CN=ldap user,CN=Users,DC=gibbsoft,DC=local";
        ldapSettings.loginPassword="Br0wser.";
        
        // Specify the three search roots; these correspond to the radio buttons
        // in the search dialog for Internal Staff, Customers and Suppliers.
        ldapSettings.internalSearchRoot="ldap://skywalker:389/ou=People,DC=gibbsoft,DC=local";
        ldapSettings.customersSearchRoot="ldap://skywalker:389/ou=People,DC=gibbsoft,DC=local";
        ldapSettings.suppliersSearchRoot="ldap://skywalker:389/ou=People,DC=gibbsoft,DC=local";
                   
        // end of LDAP connection settings
        ////////////////////////////////////////////
        setIconImage(new ImageIcon(
                getClass().getResource("icons/phuzby16x16t.gif")).getImage());

        createAndShowGUI();

        while (true) {
            dlg = new Dialog1();
            //waiting for valid search();
            while (dlg.searchString == null) {
                try {
                    Thread.sleep(250L);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            //System.out.println("Search poperty is: " + dlg.searchString);
            //System.out.println("Search scope is: " + dlg.scopeString);

            String rb = Dialog1.scopeString;

            if (rb.compareToIgnoreCase("Customers") == 0) {
                ldapSettings.currentSearchRoot = ldapSettings.customersSearchRoot;
            } else if (rb.compareToIgnoreCase("Suppliers") == 0) {
                ldapSettings.currentSearchRoot = ldapSettings.suppliersSearchRoot;
            } else if (rb.compareToIgnoreCase("Internal") == 0) {
                ldapSettings.currentSearchRoot = ldapSettings.internalSearchRoot;
            }

            //Show progress bar
            waitWindow = new Wait();
            Windows.centerOnScreen(waitWindow);
            waitWindow.setVisible(true);

            //Show Results Dialog
            SearchResults1 sr1 = new SearchResults1(ldapSettings, dlg.searchString);
            Windows.centerOnScreen(sr1);
            sr1.setVisible(true);
            waitWindow.setVisible(false);
            waitWindow = null;

        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        new Main();
    }

    private void createAndShowGUI() {
        //Check the SystemTray support
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();

        // Create a popup menu components
        MenuItem aboutItem = new MenuItem("About");
        MenuItem exitItem = new MenuItem("Exit");

        //Add components to popup menu
        popup.add(aboutItem);
        popup.addSeparator();
        popup.add(exitItem);
        trayIcon.setPopupMenu(popup);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
            return;
        }

        // ## Set the tool tip
        trayIcon.setToolTip("Double click and make someone happy with a phone call");
        
        // ## auto size the tray icon
        trayIcon.setImageAutoSize(true);
        
        // ## Double click the system tray icon
        trayIcon.addActionListener(this);
        trayIcon.setActionCommand("trayIcon");
        
        // ## About Menu Option
        aboutItem.addActionListener(this);
        
        //noneItem.addActionListener(this);
        exitItem.addActionListener(this);
    }

    //Obtain the image URL
    protected static Image createImage(String path, String description) {
        URL imageURL = Main.class.getResource(path);

        if (imageURL == null) {
            System.err.println("Resource not found: " + path);
            return null;
        } else {
            return (new ImageIcon(imageURL, description)).getImage();
        }
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        String strCommand = e.getActionCommand();

        if (strCommand.compareToIgnoreCase("About") == 0) {
            About a = new About(BVERSION);
            Windows.centerOnScreen(a);
            a.setVisible(true);
        } else if (strCommand.compareToIgnoreCase("Exit") == 0) {
            tray.remove(trayIcon);
            System.exit(0);
        } else if (strCommand.compareToIgnoreCase("trayIcon") == 0) {
            //show and hide the search window.
            if (dlg.isVisible() == true) {
                dlg.setVisible(false);
            } else {
                Windows.centerOnScreen(dlg);
                dlg.setVisible(true);
                dlg.toFront();
                dlg.setTxtfield1Focus();
            }
        }
    }
}

