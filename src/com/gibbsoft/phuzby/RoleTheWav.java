/*
 * RoleTheWav.java
 *
 * Created on 14 April 2006, 23:52
 * Copyright 2006-2008 Nigel Gibbs
 * 
 *  This file is part of Phuzby.
 *
 *  Phuzby is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Phuzby is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 * 
*/

package com.gibbsoft.phuzby;

import javax.sound.sampled.*;
import java.net.*;

/**
 *
 * @author NigelG
 */
public class RoleTheWav {
    URL url;
    
    /**
     * Creates a new instance of RoleTheWav
     */
    public RoleTheWav(URL filename) {
        url = filename;
        this.play();
        //System.out.println(this.getClass().getName());
    }
    public void play() {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(url);
            // determine the file's audio format
            AudioFormat format = ais.getFormat();
            //System.out.println("Format: " + format);
            // get a line to play the audio
            DataLine.Info info = new DataLine.Info(
                SourceDataLine.class, format);
            SourceDataLine source = (SourceDataLine) AudioSystem.getLine(
                info);
            // play the file
            source.open(format);
            source.start();
            int read = 0;
            byte[] audioData = new byte[16384];
            while (read > -1) {
                read = ais.read(audioData, 0, audioData.length);
                if (read >= 0) {
                    source.write(audioData, 0, read);
                }
            }
            source.drain();
            source.close();
        } catch (Exception exc) {
            System.out.println("Error: " + exc.getMessage());
            exc.printStackTrace();
        }

    }
}
