/*
 * Windows.java
 *
 * Created on 03 April 2006, 08:01
 * Copyright 2006-2008 Nigel Gibbs
 * 
 *  This file is part of Phuzby.
 *
 *  Phuzby is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Phuzby is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 * 
*/

package com.gibbsoft.phuzby;

/**
 *
 * @author Nigel Gibbs
 */
import java.awt.*;

public class Windows {
  public static void centerOnScreen(Window window) {
    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    window.setLocation(
      (dim.width - window.getSize().width) / 2,
      (dim.height - window.getSize().height) / 2);
  }
}
