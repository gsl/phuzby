/*
 * Person.java
 *
 * Created on 25 March 2006, 22:08
 * Copyright 2006-2008 Nigel Gibbs
 * 
 *  This file is part of Phuzby.
 *
 *  Phuzby is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Phuzby is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 * 
*/

package com.gibbsoft.phuzby;

import java.awt.*;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
import javax.swing.ImageIcon;

/**
 *
 * @author NigelG
 */
public class Person {
    
    private String cn;
    private String telephoneNumber;
    private String mobile;
    private String title;
    private String mail;
    private String department;
    private ImageIcon jpegPhoto;
    private ImageIcon thumbnailPhoto;
    
    /*
        "cn",
        "telephoneNumber",
        "mobile",
        "title",
        "mail",
        "department"
    */
    // linked list? maybe later.
    //public Person nextPerson;
    //public Person prevPerson;

        /** Creates a new instance of Person */
    public Person(SearchResult si) {
        Attributes attrs = si.getAttributes();
        Attribute tAttrib = attrs.get("cn");
        if (tAttrib != null) {
            try {
                cn = tAttrib.get().toString();
            } catch (NamingException ex) {
                ex.printStackTrace();
                cn = "";
            }
        }
        
        // ######################
        tAttrib = attrs.get("telephoneNumber");
        if (tAttrib != null) {
            try {
                telephoneNumber = tAttrib.get().toString();
           } catch (NamingException ex) {
                ex.printStackTrace();
                telephoneNumber="";
            }
        }
        
        // ######################
        tAttrib = attrs.get("mobile");
        if (tAttrib != null) {
            try {
                mobile = tAttrib.get().toString();
            } catch (NamingException ex) {
                ex.printStackTrace();
                mobile="";
            }
        }
        
        // ######################
        tAttrib = attrs.get("title");
        if (tAttrib != null) {
            try {
                title = tAttrib.get().toString();
            } catch (NamingException ex) {
                ex.printStackTrace();
                title="";
            }
        }
        
        // ######################
        tAttrib = attrs.get("mail");
        if (tAttrib != null) {
            try {
                mail = tAttrib.get().toString();
            } catch (NamingException ex) {
                ex.printStackTrace();
                mail="";
            }
        }
        
        // ######################
        tAttrib = attrs.get("department");
        if (tAttrib != null) {
            try {
                department = tAttrib.get().toString();
            } catch (NamingException ex) {
               ex.printStackTrace();
                department="";
            }
        }

        // ######################
        tAttrib = attrs.get("jpegPhoto");
        if (tAttrib != null) {
            try {
                jpegPhoto = new ImageIcon((byte[] ) tAttrib.get());
                Image i = jpegPhoto.getImage().getScaledInstance(-1,144,java.awt.Image.SCALE_SMOOTH);
                jpegPhoto.setImage(i);
                //jpegPhoto = jpegPhoto.getScaledInstance(189,144,java.awt.Image.SCALE_SMOOTH);
//                jpegPhoto = t.getScaledInstance(189,144,java.awt.Image.SCALE_SMOOTH));
                
            } catch (NamingException ex) {
               ex.printStackTrace();
                jpegPhoto=null;
            }
        } else {
            tAttrib = attrs.get("thumbnailPhoto");
            if (tAttrib != null) {
                try {
                    jpegPhoto = new ImageIcon((byte[] ) tAttrib.get());
                    Image i = jpegPhoto.getImage().getScaledInstance(-1,144,java.awt.Image.SCALE_SMOOTH);
                    jpegPhoto.setImage(i);
                    //jpegPhoto = jpegPhoto.getScaledInstance(189,144,java.awt.Image.SCALE_SMOOTH);
    //                jpegPhoto = t.getScaledInstance(189,144,java.awt.Image.SCALE_SMOOTH));

                } catch (NamingException ex) {
                   ex.printStackTrace();
                    jpegPhoto=null;
                }
            } else {
                jpegPhoto = new ImageIcon();
                ImageIcon nobody = new ImageIcon (
                    getClass().getResource( "icons/unknown_user.gif" ) );
                Image i = nobody.getImage().getScaledInstance(-1,144,java.awt.Image.SCALE_SMOOTH);
                jpegPhoto.setImage(i);
            }
        }
        
}

    public Person() {
        
    }

    public void setCn(String cn) {
        this.cn = cn;
    }
    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public void setJpegPhoto(ImageIcon jpegPhoto) {
        this.jpegPhoto = jpegPhoto;
    }
    public String getMobile() {
        return this.mobile;
    }
    public String getDepartment() {
        return this.department;
    }
    public String getTitle() {
        return this.title;
    }
    public String getCn() {
        return this.cn;
    }
    public String getTelephoneNumber() {
        return this.telephoneNumber;
    }
    public String getMail() {
        return this.mail;
    }    
    public ImageIcon getJpegPhoto() {
        return this.jpegPhoto;
    }

}
