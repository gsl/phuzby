/*
 * LdapSettings.java
 *
 * Created on 13 April 2006, 21:23
 * Copyright 2006-2008 Nigel Gibbs
 * 
 *  This file is part of Phuzby.
 *
 *  Phuzby is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  Phuzby is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package com.gibbsoft.phuzby;

/**
 *
 * @author NigelG
 */
public class LdapSettings {
    // used to init ldap connection
    public String providerURL;
    public String authenticationType;
    public String loginUser;
    public String loginPassword;
    
    // used to do the search
    public String internalSearchRoot;
    public String customersSearchRoot;
    public String suppliersSearchRoot;
    public String currentSearchRoot;
    /**
     * Creates a new instance of LdapSettings
     */
    public LdapSettings() {

    }
    
}
