/*
 * SearchResults.java
 *
 * Created on 02 April 2006, 17:41
 * Copyright 2006-2008 Nigel Gibbs
 * 
 *  This file is part of Phuzby.
 *
 *  Phuzby is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.

 *  Phuzby is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 * 
*/

package com.gibbsoft.phuzby;

//import javax.swing.DefaultListModel;
import java.awt.Desktop;
import java.text.Collator;
import javax.naming.directory.SearchResult;
import javax.swing.*;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.NamingEnumeration;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import java.util.*;
import com.gibbsoft.phuzby.RoleTheWav;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


/**
 *
 * @author  NigelG
 */
public class SearchResults1 extends javax.swing.JFrame {
    private DefaultListModel listModel;
    private Person ps[];
    public static String rAttrs[] = {
        "cn",
        "telephoneNumber",
        "mobile",
        "title",
        "mail",
        "department",
        "jpegPhoto",
        "thumbnailPhoto"
    };

    private Desktop desktop;
    private Desktop.Action action = Desktop.Action.OPEN;

    /** Creates new form SearchResults */
    public SearchResults1(LdapSettings ls, String searchString) {
        initComponents();
        setIconImage( new ImageIcon (
            getClass().getResource( "icons/phuzby16x16t.gif" ) ).getImage() );

        //####################################
        
        // Check for desktop integration support in jre 1.6 on this platform
        if (Desktop.isDesktopSupported()) {
            desktop = Desktop.getDesktop();
            // now enable buttons for actions that are supported.
            btnEmail.setEnabled(true);
        } else {
            btnEmail.setEnabled(false);
        }
        //
        
        Hashtable<Object,Object> env = new Hashtable<Object,Object>();

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

        env.put(Context.PROVIDER_URL, ls.providerURL);

        // Authenticate 
        env.put(Context.SECURITY_AUTHENTICATION, ls.authenticationType);
        env.put(Context.SECURITY_PRINCIPAL, ls.loginUser);
        env.put(Context.SECURITY_CREDENTIALS, ls.loginPassword);


        try {

            // Create the initial directory context
            DirContext ctx = new InitialDirContext(env);

            SearchControls constraints = new SearchControls();
            constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Perform search
//            NamingEnumeration answer = ctx.search(ls.currentSearchRoot
//                    , "(&(cn=*" + searchString + "*)(objectClass=person)(showInAddressBook=*))", constraints);
            NamingEnumeration answer = ctx.search(ls.currentSearchRoot
                    , "(&(cn=*" + searchString + "*)(objectClass=person))", constraints);

            Vector<Object> answers = new Vector<Object>();

            // Enumerate answers and store in a Vector for later
            // might save this to a search cache later for off-line use.
            while (answer.hasMore()) {
                SearchResult si = (SearchResult)answer.next();
                answers.addElement(si);
            }

            // loop through and add all hits into a listModel 
            listModel = new DefaultListModel();
            ps = new Person[answers.size()];
            for (int i=0;i < answers.size(); i++) {
                 SearchResult si = (SearchResult)answers.elementAt(i);
                 ps[i] = new Person(si);                 
                 listModel.addElement(ps[i].getCn());
            }
        } catch (NamingException e) {
            System.err.println("Problem getting attribute: " + e);
        }
        if (listModel != null) {
            //populate listbox
            lstResults.setModel(listModel);
            sortList(lstResults);
            lstResults.setSelectedIndex(0);
        }

        //InputMap inputMap = rootPane
        //    .getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        //KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
        //inputMap.put(stroke, "ESCAPE");
        //rootPane.getActionMap().put("ESCAPE", ActionPerformed);        
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        btnOk = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnSayExt = new javax.swing.JButton();
        btnSayMobile = new javax.swing.JButton();
        btnDialExt = new javax.swing.JButton();
        btnMobileDial = new javax.swing.JButton();
        btnEmail = new javax.swing.JButton();
        txtName = new javax.swing.JTextField();
        txtTitle = new javax.swing.JTextField();
        txtDept = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        txtExt = new javax.swing.JTextField();
        txtMobile = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstResults = new javax.swing.JList();
        photoPanel = new javax.swing.JPanel();
        lblPhoto = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Phuzby's Search Results");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnOk.setText("Ok");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Details"));
        jPanel1.setMaximumSize(new java.awt.Dimension(16, 16));
        jPanel1.setMinimumSize(new java.awt.Dimension(16, 16));
        jLabel1.setText("Name:");

        jLabel2.setText("Title:");

        jLabel3.setText("Department:");

        jLabel4.setText("Ext:");

        jLabel5.setText("Mobile:");

        jLabel6.setText("Email:");

        btnSayExt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gibbsoft/phuzby/icons/say16x16.gif")));
        btnSayExt.setToolTipText("Say");
        btnSayExt.setMaximumSize(new java.awt.Dimension(16, 16));
        btnSayExt.setMinimumSize(new java.awt.Dimension(16, 16));
        btnSayExt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSayExtActionPerformed(evt);
            }
        });

        btnSayMobile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gibbsoft/phuzby/icons/say16x16.gif")));
        btnSayMobile.setToolTipText("Say");
        btnSayMobile.setMaximumSize(new java.awt.Dimension(16, 16));
        btnSayMobile.setMinimumSize(new java.awt.Dimension(16, 16));
        btnSayMobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSayMobileActionPerformed(evt);
            }
        });

        btnDialExt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gibbsoft/phuzby/icons/phone_icon.gif")));
        btnDialExt.setToolTipText("Dial");
        btnDialExt.setBorder(null);
        btnDialExt.setMaximumSize(new java.awt.Dimension(16, 16));
        btnDialExt.setMinimumSize(new java.awt.Dimension(16, 16));
        btnDialExt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDialExtActionPerformed(evt);
            }
        });

        btnMobileDial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gibbsoft/phuzby/icons/phone_icon.gif")));
        btnMobileDial.setToolTipText("Dial");
        btnMobileDial.setBorder(null);
        btnMobileDial.setMaximumSize(new java.awt.Dimension(16, 16));
        btnMobileDial.setMinimumSize(new java.awt.Dimension(16, 16));
        btnMobileDial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMobileDialActionPerformed(evt);
            }
        });

        btnEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/gibbsoft/phuzby/icons/email.gif")));
        btnEmail.setToolTipText("Dial");
        btnEmail.setBorder(null);
        btnEmail.setMaximumSize(new java.awt.Dimension(16, 16));
        btnEmail.setMinimumSize(new java.awt.Dimension(16, 16));
        btnEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmailActionPerformed(evt);
            }
        });

        txtName.setEditable(false);

        txtTitle.setEditable(false);

        txtDept.setEditable(false);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        txtExt.setEditable(false);

        txtMobile.setEditable(false);

        txtEmail.setEditable(false);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jLabel2)
                    .add(jLabel3)
                    .add(jLabel4)
                    .add(jLabel5)
                    .add(jLabel6))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(txtTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .add(txtDept, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .add(txtName, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .add(txtExt, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .add(txtMobile, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .add(txtEmail, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(btnSayExt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnDialExt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(btnSayMobile, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnMobileDial, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(btnEmail, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jSeparator1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(txtTitle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(txtDept, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel4)
                    .add(txtExt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(8, 8, 8)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel5)
                    .add(txtMobile, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtEmail, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6)))
            .add(jPanel1Layout.createSequentialGroup()
                .add(75, 75, 75)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnSayExt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnDialExt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnSayMobile, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnMobileDial, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnEmail, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Results List"));
        lstResults.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstResults.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstResultsValueChanged(evt);
            }
        });

        jScrollPane1.setViewportView(lstResults);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                .addContainerGap())
        );

        photoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Photo"));
        lblPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        org.jdesktop.layout.GroupLayout photoPanelLayout = new org.jdesktop.layout.GroupLayout(photoPanel);
        photoPanel.setLayout(photoPanelLayout);
        photoPanelLayout.setHorizontalGroup(
            photoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(photoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(lblPhoto, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                .addContainerGap())
        );
        photoPanelLayout.setVerticalGroup(
            photoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(photoPanelLayout.createSequentialGroup()
                .add(lblPhoto, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 144, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(6, 6, 6)
                        .add(photoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnOk, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 60, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnOk)
                            .add(photoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmailActionPerformed
        String mailTo = txtEmail.getText();
        URI uriMailTo = null;
        try {
            if (mailTo.length() > 0) {
                uriMailTo = new URI("mailto", mailTo, null);
                desktop.mail(uriMailTo);
            } else {
                desktop.mail();
            }
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } catch(URISyntaxException use) {
            use.printStackTrace();
        }
        
    }//GEN-LAST:event_btnEmailActionPerformed

    private void btnMobileDialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMobileDialActionPerformed
        for (int i=0; i < txtMobile.getText().length(); i++) {
            char ch = txtMobile.getText().charAt(i);
            if (ch >= '0' && ch <= '9') {
                RoleTheWav wav = new RoleTheWav(getClass().getResource( "tdmf/" + ch + ".wav" ));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            } 
        }
    }//GEN-LAST:event_btnMobileDialActionPerformed

    private void btnDialExtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDialExtActionPerformed
        for (int i=0; i < txtExt.getText().length(); i++) {
            char ch = txtExt.getText().charAt(i);
            if (ch >= '0' && ch <= '9') {
                RoleTheWav wav = new RoleTheWav(getClass().getResource( "tdmf/" + ch + ".wav" ));
            }
        }
    }//GEN-LAST:event_btnDialExtActionPerformed

    private void btnSayMobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSayMobileActionPerformed
        for (int i=0; i < txtMobile.getText().length(); i++) {
            char ch = txtMobile.getText().charAt(i);
            if (ch >= '0' && ch <= '9') {
                RoleTheWav wav = new RoleTheWav(getClass().getResource( "voice/" + ch + ".au" ));           
            }
        }

    }//GEN-LAST:event_btnSayMobileActionPerformed

    private void btnSayExtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSayExtActionPerformed
        for (int i=0; i < txtExt.getText().length(); i++) {
            char ch = txtExt.getText().charAt(i);
            if (ch >= '0' && ch <= '9') {
                //System.out.println(getClass().getResource( "voice/" + ch + ".au" ));
                RoleTheWav wav = new RoleTheWav(getClass().getResource( "voice/" + ch + ".au" ));
            }
        }

    }//GEN-LAST:event_btnSayExtActionPerformed

    private void lstResultsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstResultsValueChanged
        String clicked = lstResults.getSelectedValue().toString();
        Person p=null;
        
        boolean found=false;
        int i=0; 
        do {
            if (ps[i].getCn().compareToIgnoreCase(clicked)==0) {
                p=ps[i];
                found=true;
            } else {
                i++;
            }
        } while (i<ps.length && found==false);
        
        txtName.setText(p.getCn());
        txtTitle.setText(p.getTitle());
        txtDept.setText(p.getDepartment());
        txtExt.setText(p.getTelephoneNumber());
        txtMobile.setText(p.getMobile());
        txtEmail.setText(p.getMail());
        lblPhoto.setIcon(p.getJpegPhoto());
    }//GEN-LAST:event_lstResultsValueChanged

    private void ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActionPerformed
        if (evt.getActionCommand().compareToIgnoreCase("Ok") == 0) {
            dispose();      
        } 
        
    }//GEN-LAST:event_ActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(final LdapSettings ls, final String searchString) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SearchResults1(ls, searchString).setVisible(true);
            }
        });
    }
    
  public void sortList(JList list) {
     DefaultListModel dlm;
     dlm = (DefaultListModel) list.getModel();
     int numItems = dlm.getSize();
     String[] a = new String[numItems];
     for (int i=0;i<numItems;i++){
       a[i] = (String)dlm.getElementAt(i);
       }
     sortArray(Collator.getInstance(),a);
     // Locale loc = Locale.FRENCH;
     // sortArray(Collator.getInstance(loc), (String[])a);
     for (int i=0;i<numItems;i++) {
       dlm.setElementAt(a[i], i);
     }
  } 

   public static void sortArray(Collator collator, String[] strArray) {
   String tmp;
   if (strArray.length == 1) return;
   for (int i = 0; i < strArray.length; i++) {
    for (int j = i + 1; j < strArray.length; j++) {
      if( collator.compare(strArray[i], strArray[j] ) > 0 ) {
        tmp = strArray[i];
        strArray[i] = strArray[j];
        strArray[j] = tmp;
        }
      }
    } 
   }

        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDialExt;
    private javax.swing.JButton btnEmail;
    private javax.swing.JButton btnMobileDial;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnSayExt;
    private javax.swing.JButton btnSayMobile;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JList lstResults;
    private javax.swing.JPanel photoPanel;
    private javax.swing.JTextField txtDept;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtExt;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtTitle;
    // End of variables declaration//GEN-END:variables
    
}
